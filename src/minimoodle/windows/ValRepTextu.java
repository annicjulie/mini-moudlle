package windows;

import java.util.ArrayList;
import java.util.HashMap;

import graphics.BackgroundMoodle;
import graphics.Footer;
import graphics.Header;
import graphics.Polices;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.Main;
import question.QRT;
import question.QuestionList;
import question.Test;
import users.Utilisateur;

public class ValRepTextu extends Stage {
	
	private QuestionList questList;
	private Polices police;
	private Utilisateur user;
	
	public ValRepTextu(QuestionList ql, Utilisateur u){
		this.setTitle("Vérification des réponses textuelles");
		this.setWidth(Main.WIDTH);
		this.setHeight(Main.HEIGHT);
		this.setResizable(Main.RESIZABLE);
		//this.bg = new BackgroundMoodle("/res/background.png", 1440, 900);
		this.questList = ql;
		this.user = u;
		this.setResizable(true);
		this.police = new Polices();
		Scene sc = new Scene(this.display());
		this.setScene(sc);
	}
	
	public Parent display() {
		Header header = new Header("Validation des réponses textuelles de " + this.user.getLogname() + " sur le test : " + this.questList.getName());
		BorderPane root = new BorderPane();
	 	Footer footer = new Footer();
		ScrollPane sp = new ScrollPane();
		VBox vb = this.affQuestionT();
		//HashMap<String, Test> hmTest = Utilisateur.getTestsForUser(this.user.getLogname());
		//Test test = hmTest.get(this.questList.getName());
		sp.setContent(vb);
		root.setBackground(new Background(new BackgroundMoodle("/res/background.png", 1440, 900).getBg()));
		root.setTop(header.getPane());
		root.setBottom(footer.getPane());
		root.setCenter(sp);
	return root;
}
	
	
	

	
	private VBox affQuestionT(){
		VBox tot = new VBox();
		Font modern = this.police.use("modern");
		ArrayList<QRT> qrtliste = this.user.getUncorrectedQuestionsForTest(this.questList.getName());
		
		for (QRT qrt : qrtliste){
			HBox temp = new HBox();
			VBox tt = new VBox();
			Button validQuest = new Button("Valider");
			Text intitule = new Text(qrt.getIntitule() + " :");
			Label rep = new Label(qrt.getReponse());
			Label score = new Label("Points :");
			rep.setFont(modern);
			score.setFont(modern);
			intitule.setFont(modern);
			System.out.println(qrt.getReponse());
			ComboBox<String> cbR = new ComboBox<String>();
			cbR.getItems().addAll("0","1");
			validQuest.setOnAction(e -> {valrep(qrt, cbR);});
			tt.getChildren().addAll(intitule,rep);
			temp.getChildren().addAll(tt, score, cbR, validQuest);
			HBox.setMargin(tt, new Insets(10, 0, 10 ,20));
			HBox.setMargin(validQuest, new Insets(5, 200, 10, 50));
			HBox.setMargin(cbR, new Insets(8,0,10,10));
			HBox.setMargin(score, new Insets(10, 0, 10, 50));
			intitule.setFill(Color.BLACK);
			intitule.setStyle("-fx-font: 20 modern;");
			rep.setTextFill(Color.BLACK);
			score.setTextFill(Color.BLACK);
			temp.setSpacing(10);
			temp.setStyle("-fx-background-color: rgb(171, 255, 219)");
			tot.getChildren().add(temp);
			VBox.setMargin(temp, new Insets(10,20,10,20));
		}
		return tot;
	}
	private void valrep(QRT qrt, ComboBox<String> listeN){
		if (listeN.getSelectionModel().getSelectedItem()=="0"){
			qrt.setScore(0);	
		}
		if (listeN.getSelectionModel().getSelectedItem()=="1"){
			qrt.setScore(1);	
		}
		this.user.writeToFS();
		Main.getTestManager().refresh();
	}
	
}

