package windows;

import graphics.BackgroundMoodle;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.Main;


/**
 * Page de connexion au miniMoodle
 * @author julie
 * @version 1.0
 */

public class LogPage extends Stage {
	
		
		private ImageView logo;
		private Rectangle back2 = new Rectangle();
		private Button button1 = new Button("Connexion");
		private Text pseudo = new Text("Pseudo:");
		private Text mdp = new Text("Mot de passe:");
		private TextField pseudo1 = new TextField("");
		private PasswordField mdp1 = new PasswordField();
		private Text erreur = new Text("Echec de la connexion");
		private VBox lecontenu = new VBox();
		private StackPane root = new StackPane();

		
		private double sw=Main.WIDTH;
		private double sh=Main.HEIGHT;
		
		private BackgroundMoodle lefond = new BackgroundMoodle("/res/background.png", 1440, 900);

		
		/**
		 * Constructeur de la classe
		 */
		public LogPage(){
			
			this.setTitle("MiniMoooodle");
			this.setResizable(Main.RESIZABLE);			
			Scene maScene = new Scene(creerContenu(),Main.WIDTH, Main.HEIGHT);
			maScene.setOnKeyPressed(e -> {enterLogin(e);});
			this.erreur.setFill(Color.RED);
			this.erreur.setVisible(false);
			this.setScene(maScene);
			this.sizeToScene();
			this.button1.setOnMouseClicked(e -> {tryLogin();});
		}
		
		
		
		
		/**
		 * Création du contenu graphique
		 */
		Parent creerContenu(){
			
			Image logo = new Image("/res/logoMinimoo.png");
			ImageView vue = new ImageView(logo);
			
			vue.setFitWidth(512*0.4);
			vue.setFitHeight(512*0.4);
			
			
			pseudo.setStyle("-fx-font: 30 arial;");
		
	
			pseudo1.setStyle("-fx-font: 30 arial;");
			
			
			mdp.setStyle("-fx-font: 30 arial;");
			
			
			mdp1.setStyle("-fx-font: 30 arial;");
			
			
		
		
			button1.setTextFill(Color.color(0, 0, 0));
			button1.setStyle("-fx-font: 30 arial;");
			
			lecontenu.setSpacing(40);
			StackPane bt =new StackPane(button1);
			StackPane loug = new StackPane(vue);
			StackPane glob = new StackPane(lecontenu);
			
		
			root.setBackground(new Background(this.lefond.getBg()));
			
			//root.setBackground(new Background(this.bg.getBg()));
			VBox vpseu1 = new VBox(pseudo,pseudo1);
			VBox vmdp1 = new VBox(mdp,mdp1);
			
			vpseu1.setSpacing(10);
			vmdp1.setSpacing(10);
			
			
			lecontenu.getChildren().addAll(loug,vpseu1,vmdp1,bt,erreur);
		
			
			glob.setBackground(new Background(new BackgroundFill(Color.color(1, 1, 1, 0.5), null, null)));
			
			root.getChildren().addAll(glob);
			StackPane.setMargin(glob, new Insets(400,400,400,400));
			StackPane.setMargin(lecontenu, new Insets(20,20,20,20));
			StackPane.setMargin(loug, new Insets(0,0,0,0));
			
			return root;
		}
		
		
		
		/**
		 * méthode qui permet la connexion
		 */
		private void tryLogin() {
			if(Main.getConnectionManager().connect(this.pseudo1.getText(), this.mdp1.getText())) {
				if(Main.getConnectionManager().getCurrentUser().isProf())
					Main.getSceneManager().changeScene(new AccueilProfs());
				else {
					Main.getSceneManager().changeScene(new AccueilEleves());
				}
			} else this.erreur.setVisible(true);
		}
		
		
		
		/**
		 * méthode qui lance trylogin() si le curseur est sur le mot de passe et l'utilisateur appuie su entré
		 */
		private void enterLogin(KeyEvent e) {
			if (e.getCode() == KeyCode.ENTER && this.mdp1.isFocused()) {
				tryLogin();
			}
		}
		
}