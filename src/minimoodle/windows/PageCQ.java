package windows;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import question.Choix;

public class PageCQ {
	
	private VBox box = new VBox();
	private Tab q = new Tab();
	private VBox root = new VBox();
	private HBox tog = new HBox();
	private TextArea intiq = new TextArea();
	private RadioButton r1 = new RadioButton("Réponse textuelle");
	private RadioButton r2 = new RadioButton("QCM");
	private Text afnbq = new Text("Intitulé de la question:");
	private Text typeQ = new Text("Type de question");
	private ToggleGroup choixTypeQ = new ToggleGroup();
	private Button ajoutR = new Button("Ajouter une reponse");
	private ArrayList<CheckBox> res = new ArrayList<CheckBox>();
	private ArrayList<TextField> rep = new ArrayList<TextField>();
	private VBox reps = new VBox();
	
	
	/**@author Julie
	 * classe PageCQ
	 * Elle crée des onglets(questions) qui seront ensuite ajouté à la classe Création questionnaire
	 * @version 1.0
	 * @param numero de la question au sein du questionnaire
	 */
	
	
	public PageCQ(int n){
			
			creationPage(n);

		
		
		this.r2.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>(){
			public void handle(ActionEvent e) {
				
		
				
				
				nouvellereponse();
				nouvellereponse();
			
				
			}
		});
		
		
		this.r1.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>(){
			public void handle(ActionEvent e) {
				
				res.clear();
				rep.clear();
				reps.getChildren().clear();
			
				

				
			}
		});
		
		this.ajoutR.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>(){
			public void handle(ActionEvent e) {
				
				nouvellereponse();
				
			}
		});
		
	}
	
	public void creationPage(int n){
	
			this.q.setText("Question "+n);
			this.box.getChildren().add(question());
			
			q.setContent(this.box);
		
		
		CreationQuestionnaire.newtab(q);
	}

	/**
	 * Création du contenu dans l'inglet de la question
	 */
	Parent question(){
		
		this.root.setSpacing(20);		
		this.tog.setSpacing(80);

		
		this.intiq.setMinWidth(600);
		this.intiq.setMinHeight(110);
		
		
		this.r1.setToggleGroup(choixTypeQ);
		this.r2.setToggleGroup(choixTypeQ);
		
		this.r1.setMinWidth(200);
		this.r2.setMinWidth(200);
		this.r1.setSelected(true);
		
		this.reps.setMinHeight(300);
		this.reps.setSpacing(20);
		
		
	
		this.tog.getChildren().addAll(r1,r2);
		this.root.getChildren().addAll(afnbq,intiq,typeQ,tog,reps);
		
	
		return root;

		
	}
		
	/**
	 * methode permettant de créer une nouvelle réponse
	 */
		public void nouvellereponse(){
			reps.getChildren().remove(ajoutR);
			if (res.size()<6){
				CheckBox c =new CheckBox();
				TextField t= new TextField("");
				t.setMinWidth(500);
				HBox reponse = new HBox();
				reponse.getChildren().addAll(c,t);
				this.reps.getChildren().addAll(reponse);
				this.res.add(c);
				this.rep.add(t);
				
				
			}
			
			reps.getChildren().addAll(ajoutR);
			
		}

		/**
		 * @param rien
		 * @return l'intitulé de la question
		 */
		public String getIntiq() {
			return intiq.getText();
		}
		
		
		
		/**
		 * @param rien
		 * @return true si la responses est textuelles ou false si c'est un qcm
		 */
		public boolean getType(){
			boolean type=r1.isSelected();
			
			return type;
		}
		
		
		
		/**
		 * @param rien
		 * @return un booleen indiquant si le qcm est à réponse multiple ou unique
		 */
		public boolean getnbres(){
			boolean xor=true;
			int nbv=0;
			
			for (int i=0;i<res.size();i++){
				CheckBox cb= res.get(i);
				if(cb.isSelected()==true){
					nbv++;
					
				}
				
			}
			
			if (nbv>1){
				
				xor=false;
				
			}
			
			return xor;
		}
		
		
		
		
		/**
		 * @param rien
		 * @return une arraylist contenant les reponses d'un qcm
		 */
		public ArrayList<Choix> getlesrep(){
			ArrayList<Choix> lesreps= new ArrayList<Choix>();
			
			for (int i=0;i<rep.size();i++){
				CheckBox cb= res.get(i);
				
				String intrep=rep.get(i).getText();
				
				boolean val=cb.isSelected();
			
				
				Choix c =new Choix(intrep, val);
				lesreps.add(c);
				
				//System.out.println(lesreps);
					
				
			}
			
			return lesreps;
			
		}
		
		
		/**
		 * methode verifiant que des champs de réponses ne sont pas laissé vides.
		 * Appellée avant de valider le questionnaire
		 * @return vrai si un champ est laissé vide
		 */
		
		public boolean repsvides(){
			boolean res= false;
			
			for (int i=0;i<rep.size();i++){
				if(rep.get(i).getText().equals("")){
					res=true;
				}
				
			}
			
			return res;
		}
		
		
		/**
		 * methode verifiant le nombre de réponses cochés .
		 * Appellée avant de valider le questionnaire
		 * @return vrai si aucune réponse n'est coché
		 */
		public boolean getnbresv(){
			boolean resu=false;
			int nbv=0;
			
			for (int i=0;i<res.size();i++){
				CheckBox cb= res.get(i);
				if(cb.isSelected()==true){
					nbv++;
					
				}
				
			}
			
			if (nbv==0){
				
				resu=true;
				
			}
			
			return resu;
			
		}
		

}