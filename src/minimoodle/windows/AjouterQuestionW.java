package windows;

import java.util.ArrayList;
import java.util.HashMap;

import graphics.BackgroundMoodle;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;
import question.Choix;
import question.QCM;
import question.QRT;

/**
 * Classe de la fenêtre ajouter question
 * @author leguyaai
 * @version 1.0
 * 
 */

public class AjouterQuestionW extends Stage {

	private VBox root;
	private GridPane saisieArea;
	private HBox buttonsArea;
	private TextField intitule;
	private Button bnValider;
	private Button bnAnnuler;
	private BackgroundMoodle bg;
	private ToggleGroup tg;
	private RadioButton qcm;
	private RadioButton qrt;
	private Button moreChoice;
	private VBox qcmChoixComp;
	private HBox qcmChoix;
	private ArrayList<Choix> choixOfQuest;
	private ArrayList<CheckBox> cbChoixList;
	private ArrayList<TextField> tfChoixList;
	private HashMap<Choix, TextField> hmTf;
	private HashMap<Choix, CheckBox> hmCb;
	private HashMap<Choix, Button> hmBn;
	private HashMap<Choix, HBox> hmHBox;
	private CheckBox cbXor;
	private boolean isQCMXor;
	private Label sizeExceded;
	private Label qlabel;
	private Label clabel;
	private TestModificationW tfw;
	
	/**
	 * Constructeur de la classe AjouterQuestionW qui prend en paramètre effectif une fenêtre TetsModficationW
	 * @version 1.0
	 * @param tfw TestMofificationW
	 */
	
	public AjouterQuestionW(TestModificationW tfw) {
		this.setTitle("Ajouter question");
		this.setWidth(600);
		this.setHeight(600);
		this.setResizable(Main.RESIZABLE);
		this.choixOfQuest = new ArrayList<Choix>();
		this.cbChoixList = new ArrayList<CheckBox>();
		this.tfChoixList = new ArrayList<TextField>();
		this.hmCb = new HashMap<Choix, CheckBox>();
		this.hmTf = new HashMap<Choix, TextField>();
		this.hmBn = new HashMap<Choix, Button>();
		this.hmHBox = new HashMap<Choix, HBox>();
		this.tfw = tfw;
		this.isQCMXor = false;
		this.bg = new BackgroundMoodle("/res/background2.png", 550, 404);
		this.initModality(Modality.APPLICATION_MODAL);
		Scene sc = new Scene(this.display());
		this.setScene(sc);
	}
	
	
	/**
	 * Cette méthode va être appelée par le constructeur pour initialiser la fenêtre.
	 * C'est elle qui va placer et gérer tout les éléments de la fenêtre.
	 * @author leguyaai
	 * @since 1.0 
	 * @return Parent Le groupe racine
	 */
	
	public Parent display() {
		this.root = new VBox(); //Groupe principal
		this.saisieArea = new GridPane();
		this.buttonsArea = new HBox();
		this.qcmChoixComp = new VBox();
		this.qlabel = new Label("Question : ");
		this.clabel = new Label("Choix :");
		this.tg = new ToggleGroup();
		this.sizeExceded = new Label();
		this.qcm = new RadioButton("QCM");
		this.qrt = new RadioButton("QRT");
		this.qcm.setToggleGroup(this.tg);
		this.qrt.setToggleGroup(this.tg);
		this.tg.selectToggle(this.qrt);
		this.qcm.setOnAction(e -> {qcmAction();});
		this.qrt.setOnAction(e -> {qrtAction();});
		this.moreChoice = new Button("+");
		this.moreChoice.setDisable(true);
		this.moreChoice.setOnAction(e -> {addChoix();});
		this.sizeExceded.setTextFill(Color.RED);
		
		this.bnAnnuler = new Button("Annuler");
		this.bnValider = new Button("Valider");
		this.bnAnnuler.setOnAction(e -> {fermer();});
		this.bnValider.setOnAction(e -> {valider();});
		this.bnValider.setDisable(false);
		this.intitule = new TextField();
		this.qcmChoixComp.setSpacing(10);
		
		this.saisieArea.add(this.qlabel, 0, 0);
		this.saisieArea.add(this.intitule, 1, 0);
		this.saisieArea.add(this.qcm, 0, 1);
		this.saisieArea.add(this.qrt, 1, 1);
		this.saisieArea.add(this.clabel, 0, 2);
		this.saisieArea.add(this.moreChoice, 1, 2);
		this.buttonsArea.getChildren().addAll(this.bnValider, this.bnAnnuler);
		root.getChildren().addAll(this.saisieArea, this.qcmChoixComp, this.sizeExceded, this.buttonsArea);
		
		this.saisieArea.setHgap(5);
		this.saisieArea.setVgap(10);
		VBox.setMargin(saisieArea, new Insets(20, 20, 20, 20));
		VBox.setMargin(buttonsArea, new Insets(20, 20, 20, 20));
		buttonsArea.setAlignment(Pos.CENTER);
		buttonsArea.setSpacing(15);
		
		root.setBackground(new Background(this.bg.getBg()));
		return root;
	}
	
	/**
	 * Manager de l'action du RadioButton QCM
	 * @since 1.0
	 */
	
	private void qcmAction() {
		this.cbXor = new CheckBox("Choix exclusif");
		this.cbXor.setOnAction(event -> {qcmXorCheck();});
		this.moreChoice.setDisable(false);
		GridPane.setColumnIndex(this.clabel, 0);
		GridPane.setRowIndex(this.clabel, 3);
		GridPane.setColumnIndex(this.moreChoice, 1);
		GridPane.setRowIndex(this.moreChoice, 3);
		this.saisieArea.add(this.cbXor, 0, 2);
	}
	
	/**
	 * Cette méthode gère le boolean représentant le fait que la question de type QCM doit avoir des réponses à choix exclusif ou non
	 * @since 1.0
	 */
	
	private void qcmXorCheck() {
		if (this.cbXor.isSelected()) {
			this.isQCMXor = true;
			}
		else {
			this.isQCMXor = false;
			}
		}
	
	/**
	 * Manager de l'action du RadioButton QRT
	 * @since 1.0
	 */
	
	private void qrtAction() {
		this.moreChoice.setDisable(true);
		this.saisieArea.getChildren().remove(this.cbXor);
		GridPane.setColumnIndex(this.clabel, 0);
		GridPane.setRowIndex(this.clabel, 2);
		GridPane.setColumnIndex(this.moreChoice, 1);
		GridPane.setRowIndex(this.moreChoice, 2);
		
	}
	
	/**
	 * Cette méthode ferme la fenêtre
	 * @since 1.0	 * 
	 */
	
	private void fermer() {
		this.close();
	}
	
	/**
	 * Cette méthode retourne le booléen qui représente la question à choix exclusif
	 * @since 1.0
	 * @return boolean
	 */
	
	public boolean isQCMXor() {
		return this.isQCMXor;
	}
	
	/**
	 * Cette méthode supprime un choix que l'on aurait ajouté au préalable, elle est appelée par un bouton
	 * @since 1.0
	 * @param c Choix
	 */
	
	private void deleteChoix(Choix c) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.getDialogPane().setMinHeight(this.getHeight()/3);
		alert.setContentText("Êtes vous sûr de vouloir supprimer ce choix ?");
		alert.showAndWait().ifPresent(response -> {
		     if (response == ButtonType.OK) {
				this.qcmChoixComp.getChildren().remove(this.hmHBox.get(c));
		    	 this.choixOfQuest.remove(c);
		     }
		 });
	}
	
	/**
	 * Cette méthode permet d'ajouter un nouveau choix, elle appelée par un bouton
	 * @since 1.0
	 */
	
	private void addChoix() {
		this.qcmChoix = new HBox();
		TextField tf = new TextField();
		CheckBox cb = new CheckBox();
		Button bn = new Button("X");
		Choix choix = new Choix("", false);
		
		if (this.cbChoixList.size() < 5) {
			this.cbChoixList.add(cb);
			this.tfChoixList.add(tf);
			this.choixOfQuest.add(choix);
			
			this.hmCb.put(choix, cb);
			this.hmTf.put(choix, tf);
			this.hmBn.put(choix, bn);
			bn.setOnAction(event -> {deleteChoix(choix);});
			
			this.qcmChoix.getChildren().addAll(tf, cb, bn);
			this.hmHBox.put(choix, this.qcmChoix);
			this.qcmChoixComp.getChildren().add(this.qcmChoix);
			this.qcmChoix.setSpacing(5);
		}
		else {
			this.sizeExceded.setText("Vous ne pouvez pas ajouter plus de 5 choix à une question");
		}
		HBox.setMargin(cb, new Insets(3, 0, 0, 0));
		VBox.setMargin(this.qcmChoix, new Insets(0, 0, 0, 15));
	}
	
	/**
	 * Cette méthode renvoie si plusieurs cases ont été cochées, et gère les cas d'erreurs en indiquant où l'utilisateur s'est trompé
	 * @since 1.0
	 * @param qcm QCM
	 * @return boolean
	 */
	
	private boolean isMultiCheckBoxChecked(QCM qcm) {
		boolean checked = false;
		int i = 0;
		for (Choix c : this.choixOfQuest) {
			if (this.hmCb.get(c).isSelected()) {
				i++;
			}
		}
		if (i > 1) {
			checked = true;
			for (Choix c : this.choixOfQuest) {
				if (this.hmCb.get(c).isSelected()) {
					this.hmTf.get(c).setStyle("-fx-text-fill: red;");
					this.hmCb.get(c).setStyle("-fx-border-color: red; -fx-border-style: solid;");
				}
			}
		}
		else {
			for (Choix c : qcm.getChoix()) {
					this.hmTf.get(c).setStyle("-fx-text-fill: black;");
					this.hmCb.get(c).setStyle("-fx-border-color: black; -fx-border-style: hidden;");
				}
		}
		return checked;
	}
	
	
	/** Cette méthode est appelée par le bouton Valider et s'occupe d'enregistrer la nouvelle question et de l'ajouter à la liste du test modifié.
	 * @since 1.0
	 */
	
	private void valider() {
		RadioButton rb = (RadioButton) this.tg.getSelectedToggle();
		boolean inConfirmation = false;
		if (rb.getText().equalsIgnoreCase("QCM")) {
			for (Choix c : this.choixOfQuest) {
				c.setCorrectState(this.hmCb.get(c).isSelected());
				c.setIntitule(this.hmTf.get(c).getText());
			}
			QCM qcm = new QCM(this.intitule.getText(), this.cbXor.isSelected(), this.choixOfQuest);
			if (this.isQCMXor && !isMultiCheckBoxChecked(qcm)) {
				this.tfw.getQuestionList().addQuestion(qcm, this.tfw.getQuestionList().size());
			}
			else if (!this.isQCMXor) {
				for (Choix c : this.choixOfQuest) {
					c.setCorrectState(this.hmCb.get(c).isSelected());
					c.setIntitule(this.hmTf.get(c).getText());
				}
				QCM qcm1 = new QCM(this.intitule.getText(), this.cbXor.isSelected(), this.choixOfQuest);
				this.tfw.getQuestionList().addQuestion(qcm1, this.tfw.getQuestionList().size());
			}
			else {
				inConfirmation = true;
				Alert alert = new Alert(AlertType.ERROR);
				alert.getDialogPane().setMinHeight(this.getHeight()/3);
				alert.setContentText("Vous ne pouvez pas attribuer plus de une bonne r�ponse aux QCM dont le choix est exclusif");
				alert.showAndWait().ifPresent(response -> {
				     if (response == ButtonType.OK) {
				    	 
				     }
				 });
			}
		}
		else {
			QRT qrt = new QRT(this.intitule.getText());
			this.tfw.getQuestionList().addQuestion(qrt, this.tfw.getQuestionList().size());
		}
		if (!inConfirmation) {
			this.close();
			this.tfw.refresh();
		}
	}
}
