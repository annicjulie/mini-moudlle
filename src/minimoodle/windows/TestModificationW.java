package windows;

import java.util.ArrayList;
import java.util.HashMap;

import graphics.BackgroundMoodle;
import graphics.Footer;
import graphics.Header;
import graphics.Polices;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import main.Main;
import question.Choix;
import question.QCM;
import question.QRT;
import question.Question;
import question.QuestionList;

/**
 * Classe de la fenêtre de modification d'un test
 * @author leguyaai
 * @version 1.0
 * 
 */

public class TestModificationW extends Stage {
	
	private VBox intiQuest;
	private VBox core;
	private HBox btnCore;
	private Label testName;
	private Button valider;
	private Button ajouter;
	private QuestionList qL;
	private ScrollPane scrollpane;
	private ArrayList<TextField> iList;
	private HashMap<Choix, CheckBox> cbList;
	private HashMap<Choix, TextField> tfList;
	private BackgroundMoodle bg;
	private Polices polices;
	private Font clem;
	private Font modern;
	private int indexOfQl;
	private ArrayList<QuestionList> qlList;
	
	/**
	 * C'est le constructeur de la fenêtre TestModificationW, cette fenêtre va permettre à un professeur
	 * de modifier un test.
	 * @author leguyaai
	 * @since 1.0 
	 * @param ql QuestionList. C'est le test à modifier
	 */
	
	public TestModificationW(QuestionList ql) {
		this.qlList = QuestionList.readFromFS();
		this.indexOfQl = this.qlList.indexOf(ql);
		System.out.println("Index : " + this.indexOfQl);
		this.setTitle("Modification de questionnaire");
		this.setWidth(Main.WIDTH);
		this.setHeight(Main.HEIGHT);
		this.setResizable(Main.RESIZABLE);
		this.polices = new Polices();
		this.iList = new ArrayList<TextField>();
		this.cbList = new HashMap<Choix, CheckBox>();
		this.tfList = new HashMap<Choix, TextField>();
		this.clem = this.polices.use("clem");
		this.modern = this.polices.use("clem");
		this.bg = new BackgroundMoodle("/res/background2.png", 1440, 900);
		this.qL = ql;
		this.testName = testName(this.testName);
		Scene sc = new Scene(this.display());
		this.setScene(sc);
	}
	
	
	/**
	 * Cette méthode va être appelée par le constructeur pour initialiser la fenêtre.
	 * C'est elle qui va placé et gérer tout les éléments de la fenêtre.
	 * @since 1.0 
	 * @return Parent - Le groupe racine
	 */
	
	public Parent display() {
		this.scrollpane = new ScrollPane();
		this.scrollpane.setHbarPolicy(ScrollBarPolicy.NEVER);
		BorderPane root = new BorderPane();
		Header header = new Header(this.testName.getText());
		Footer footer = new Footer();
		this.intiQuest = new VBox();
		this.core = new VBox();
		this.btnCore = new HBox();
		HBox labels = new HBox();
		Label l1 = new Label("Questions :");
		Label l2 = new Label("R�ponses :");
		l1.setFont(this.clem);
		l2.setFont(this.modern);
		labels.setSpacing(350);
		labels.getChildren().addAll(l1, l2);
		
		this.valider = Valider(this.valider);
		this.valider.setOnAction(e -> {Validate();});
		this.ajouter = new Button("Ajouter question");
		this.ajouter.setOnAction(e -> {ouvrirAjouterQuestion(this);});
		this.ajouter.setPrefSize(150, 50);
		this.intiQuest = setupModification();
		this.scrollpane.setContent(this.intiQuest);
		this.scrollpane.setStyle("-fx-background: rgb(171, 255, 219);");
		this.core.setSpacing(20);
		this.btnCore.getChildren().addAll(this.valider, this.ajouter);
		this.core.getChildren().addAll(labels, this.scrollpane, this.btnCore);
		this.btnCore.setAlignment(Pos.BOTTOM_CENTER);
		this.intiQuest.setSpacing(5);
		this.core.setPadding(new Insets(45, 70, 45, 20));
		this.btnCore.setSpacing(10);
		this.btnCore.setPadding(new Insets(50, 0, 0, 0));
		root.setTop(header.getPane());
		root.setBottom(footer.getPane());		
		root.setLeft(this.core);
		root.setBackground(new Background(this.bg.getBg()));
		return root;
	}
	
	/**
	 * Cette méthode permet de mettre en place les objets qui serviront à modifier le test.
	 * @since 1.0
	 * @return intiQuest VBox
	 */
	
	private VBox setupModification() {
		for (Question q : this.qL.getQuestions()) {
			HBox temp = new HBox();
			Button validQuest = new Button("Valider");
			Button validResp = new Button("Valider");
			Button deleteQuest = new Button("X");
			Label lab = new Label(String.valueOf(this.qL.getQuestions().indexOf(q) + 1));
			Label type = new Label("Type : " + q.getType());
			TextField intitule = Intitule(q.getIntitule());
			validQuest.setOnAction(e -> {modifIntitBtn(q);});
			validResp.setOnAction(e -> {modifRespBtn(q);});
			deleteQuest.setOnAction(e -> {deleteQuestion(q);});
			temp.getChildren().addAll(lab, intitule, validQuest, type);
			VBox v2 = setupAnswers(q);
			temp.getChildren().addAll(v2, deleteQuest);
			if (q.getType() != "QRT") temp.getChildren().add(validResp);
			this.iList.add(intitule);
			HBox.setMargin(intitule, new Insets(14, 0, 0 ,0));
			HBox.setMargin(type, new Insets(16, 0, 0, 0));
			HBox.setMargin(validQuest, new Insets(14, 0, 0, 0));
			HBox.setMargin(lab, new Insets(18, 8, 0, 0));
			HBox.setMargin(validResp, new Insets(13, 40, 0, 0));
			HBox.setMargin(deleteQuest, new Insets(14, 0, 0, 0));
			temp.setSpacing(5);
			VBox.setMargin(temp, new Insets(0, 0, 20, 0));
			this.intiQuest.getChildren().add(temp);
		}
		return this.intiQuest;
	}
	
	/**
	 * Cette méthode permet de mettre en place l'espace de modification des réponses du test.
	 * @since 1.0
	 * @param q Question
	 * @return vbox VBox
	 */
	
	private VBox setupAnswers(Question q) {
		VBox vbox = new VBox();
		FlowPane fp = new FlowPane();
		if (q.getType() == "QCM") {
			QCM qcm = (QCM) q;
			for (Choix c : qcm.getChoix()) {
				CheckBox cb = new CheckBox();
				TextField tfC = new TextField(c.getIntitule());
				this.cbList.put(c, cb);
				this.tfList.put(c, tfC);
				if (c.getCorrectState()) cb.setSelected(true);
				tfC.setPrefWidth(100);
				fp.getChildren().addAll(tfC, cb);
				FlowPane.setMargin(tfC, new Insets(13, 10, 10, 10));
			}
		}
		fp.setPrefWrapLength(500);
		vbox.getChildren().addAll(fp);
		VBox.setMargin(fp, new Insets(0, 0, 0, 152));
		return vbox;
	}
	
	/**
	 * Cette méthode permet d'ouvrir la fenêtre AjouterQuestionW qui est appelée lorsqu'un bouton est actionné
	 * @since 1.0
	 * @param tfw TetsModificationW
	 */
	
	private void ouvrirAjouterQuestion(TestModificationW tfw) {
		AjouterQuestionW window = new AjouterQuestionW(tfw);
		window.show();
	}
	
	/**
	 * Cette méthode est appelée par un bouton valider réponses. Elle permet de valider les réponses d'une question du test.
	 * @since 1.0
	 * @param q Question
	 */
	
	private void modifRespBtn(Question q) {
		QCM qcm = (QCM) q;
		if (qcm.isXor() && !isMultiCheckBoxChecked(qcm)) {
			for (Choix c : qcm.getChoix()) {
				c.setIntitule(this.tfList.get(c).getText());
				if (this.cbList.get(c).isSelected()) c.setCorrectState(true);
				else if (!this.cbList.get(c).isSelected()) c.setCorrectState(false);
			}
		}
		else if (!qcm.isXor()) {
			for (Choix c : qcm.getChoix()) {
				c.setIntitule(this.tfList.get(c).getText());
				if (this.cbList.get(c).isSelected()) c.setCorrectState(true);
				else if (!this.cbList.get(c).isSelected()) c.setCorrectState(false);	
			}
		}
		else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.getDialogPane().setMinHeight(this.getHeight()/3);
			alert.setContentText("Vous ne pouvez pas attribuer plus de une bonne réponse aux QCM dont le choix est exclusif");
			alert.showAndWait().ifPresent(response -> {
			     if (response == ButtonType.OK) {
			    	 Main.logDebug(qcm.getIntitule());
					for (Choix c : qcm.getChoix()) {
						Main.logDebug("\t" + c.getIntitule() + " : " + String.valueOf(c.getCorrectState()));
					}
			     }
			 });
		}
	}
	
	/**
	 * Cette méthode sert à supprimer une question du test. Elle est appelée par un bouton.
	 * @since 1.0
	 * @param q Question
	 */
	
	private void deleteQuestion(Question q) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setContentText("Êtes vous sûr de vouloir supprimer cette question du test " + this.qL.getName() + " ?");
		alert.showAndWait().ifPresent(response -> {
		     if (response == ButtonType.OK) {
		    	 this.qL.getQuestions().remove(q);
		 		refresh();
		     }
		 });
		Validate();
	}
	
	/**
	 * Cette méthode permet de mettre à jour la fenêtre pour afficher les bonnes questions.
	 * @since 1.0
	 */
	
	public void refresh() {
		Stage stage = new TestModificationW(this.qL);
		Main.getSceneManager().changeScene(stage);
	}
	
	/**
	 * Cette méthode renvoie si plusieurs cases ont été cochées, et gère les cas d'erreurs en indiquant où l'utilisateur s'est trompé.
	 * @since 1.0
	 * @param qcm Question
	 * @return boolean
	 */
	
	private boolean isMultiCheckBoxChecked(QCM qcm) {
		boolean checked = false;
		int i = 0;
		for (Choix c : qcm.getChoix()) {
			if (this.cbList.get(c).isSelected()) {
				i++;
			}
		}
		if (i > 1) {
			checked = true;
			for (Choix c : qcm.getChoix()) {
				if (this.cbList.get(c).isSelected()) {
					this.tfList.get(c).setStyle("-fx-text-fill: red;");
					this.cbList.get(c).setStyle("-fx-border-color: red; -fx-border-style: solid;");
				}
			}
		}
		else {
			for (Choix c : qcm.getChoix()) {
					this.tfList.get(c).setStyle("-fx-text-fill: black;");
					this.cbList.get(c).setStyle("-fx-border-color: black; -fx-border-style: hidden;");
				}
		}
		return checked;
	}
	
	/**
	 * Cette méthode valide la modification de l'intitulé d'une question. Elle est appelée par un bouton
	 * @since 1.0
	 * @param q Question
	 */
	
	private void modifIntitBtn(Question q) {
		q.setIntitule(this.iList.get(this.qL.getQuestions().indexOf(q)).getText());
	}
	
	/**
	 * Cette méthode génère le TextField qui va accueillir l'intitulé des questionsdu test.
	 * @since 1.0
	 * @param title String
	 * @return TextField
	 */
	
	private TextField Intitule(String title) {
		TextField tf = new TextField();
		tf.setText(title);
		tf.setPrefWidth(275);
		return tf;
	}
	
	/**
	 * Cette méthode permet de valider l'ensemble de la modification du test. Elle s'occupe d'actionner l'ensemble des boutons valider.
	 * @since 1.0
	 */
	
	private void Validate() {
		for (Question q : this.qL.getQuestions()) {
			if (q.getType().equalsIgnoreCase("QCM")) modifRespBtn(q);
			modifIntitBtn(q);
		}
		for (Question q : this.qL.getQuestions()) {
			if (q.getType().equalsIgnoreCase("QCM")) {
				QCM qcm = (QCM) q;
				Main.logDebug(qcm.getIntitule());
				for (Choix c : qcm.getChoix()) {
					Main.logDebug("\t" + c.getIntitule() + " : " + String.valueOf(c.getCorrectState()));
				}
			}
			else {
				QRT qrt = (QRT) q;
				Main.logDebug((qrt.getIntitule()));
			}
		}
		try {
			QuestionList.setQuestionInList(this.indexOfQl, this.qL);
			QuestionList.writeToFS();
		} catch(Exception ex) {ex.printStackTrace();;}
		System.out.println("----------------\n------------------------\n\n");
		
	}

	
	/**
	 * Cette méthode renvoie un bouton valider.
	 * @since 1.0
	 * @param b - Button
	 * @return Button
	 **/
	private Button Valider(Button b) {
		b = new Button("Valider");
		b.setPrefSize(150, 50);
		return b;
	}
	
	
	/**
	 * Cette méthode renvoie un label contenant le nom du test.
	 * @since 1.0
	 * @param label - Label
	 * @return Label
	 **/
	
	private Label testName(Label label) {
		label = new Label("Modification du test : " + this.qL.getName());
		label.setFont(new Font(27));
		return label;
	}
	
	/**
	 * Cette méthode renvoie le test qui est en train d'être modifier.
	 * @return QuestionList ql
	 */
	
	public QuestionList getQuestionList() {
		return this.qL;
	}

}