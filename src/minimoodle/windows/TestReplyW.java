package windows;

import graphics.BackgroundMoodle;
import graphics.Footer;
import graphics.Header;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import main.Main;
import question.Test;
import users.Utilisateur;

public class TestReplyW extends Stage {

	private Test test;
	private Utilisateur user;
	private BorderPane root;
	private Button next;
	
	public TestReplyW(Utilisateur u, Test t) {
		this.setTitle("Réponse a un questionnaire");
		this.setWidth(Main.WIDTH);
		this.setHeight(Main.HEIGHT);
		this.setResizable(Main.RESIZABLE);
		this.test = t;
		this.user = u;
		Scene sc = new Scene(this.display());
		this.setScene(sc);
	}
	
	/**
	 * cree le contenu de la fenetre
	 * @return le contenu JavaFx
	 */
	private Parent display() {
		root = new BorderPane();
		
		Header header = new Header(this.test.getName());
		root.setTop(header.getPane());
		
		Group content = new Group();
		Label l = new Label("Pret ? Commençons le test !");
		content.getChildren().add(l);
		
		this.next = new Button("Suivant");
		this.next.setOnMouseClicked(e -> {this.next();});
		
		Footer footer = new Footer();
		root.setBottom(footer.getPane());
		footer.getPane().getChildren().add(next);
		footer.getPane();
		StackPane.setMargin(next, new Insets(0, 20, 0, 0));
		footer.getPane().setAlignment(Pos.CENTER_RIGHT);
		
		root.setBackground(new Background(new BackgroundMoodle("/res/background.png", 1440, 900).getBg()));
		root.setCenter(content);
		
		return root;
	}
	
	/**
	 * affiche la question suivante, ou un message indiquant que le test est termine
	 * gere la sauvegarde des resultats dans le systeme de fichiers via le TestManager
	 */
	private void next() {
		this.test.nextQuestion();
		root.setCenter(this.test.getGraphicContent());
		if(this.test.isTerminated()) {
			this.user.addTest(this.test);
			this.user.writeToFS();
			this.next.setText("Terminer");
			Main.getTestManager().refresh();
			this.next.setOnMouseClicked(e -> {
				Main.getSceneManager().changeScene(Main.getConnectionManager().getAccueil());
			});
		}
	}
}
