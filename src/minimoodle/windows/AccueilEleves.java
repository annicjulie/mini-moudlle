package windows;

import java.util.HashMap;

import graphics.BackgroundMoodle;
import graphics.Footer;
import graphics.Header;
import graphics.Polices;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import main.Main;
import question.QuestionList;
import question.Test;
import users.Utilisateur;

public class AccueilEleves extends Stage{
	
	private Polices police = new Polices();
	private Label colonneTest = new Label("- Tests -");
	private Label colonneRes = new Label("- Notes -");
	private BackgroundMoodle bg = new BackgroundMoodle("/res/background.png", 1440, 900);
	HashMap<String, Test> tests = Utilisateur.getTestsForUser(Main.getConnectionManager().getCurrentUser().getLogname());//obtenir tous les tests de l'utilisateur
	
	
	/**
	 * Constructeur de la classe
	 * @author rouxsi
	 * @version 1.0
	 */
	public AccueilEleves(){
		this.setTitle("Accueil étudiants");// titre de la fenetre
		this.setResizable(Main.RESIZABLE);// on peut modifier la taille de la fenetre
		Scene laScene = new Scene(creerContenu(), Main.WIDTH, Main.HEIGHT);
		this.setScene(laScene);
		this.sizeToScene();
	}
	/**
	 * Permet la création du contenu graphique pour la page d'accueil des profils élèves
	 * @author rouxsi
	 * @version 1.0
	 **/
	Parent creerContenu(){
		// le conteneur principal
		BorderPane root = new BorderPane();
		
		//le header
		Header header = new Header("Accueil Étudiants");
		
		//le footer
		Footer footer = new Footer();
				
		//le contenu de la fenetre
		GridPane fenetre = new GridPane();
				
		// la police
		Font modern = police.use("modern");
		Font clem = police.use("clem");
		colonneTest.setFont(clem);
		colonneRes.setFont(clem);
		
		fenetre.setHgap(50); // taille minimale d'une case
		fenetre.setVgap(10); // taille minimale d'une case
		
		fenetre.add(colonneTest, 3, 4,2,1);// on ajoute le texte -Tests- 
		fenetre.add(colonneRes, 10, 4,2,1); // on ajoute le texte -Résultats-
		
		int i = 0;
		/**
		 * Affichage des tests avec leur note
		 * @author rouxsi
		 * @version 1.0
		 */
		for (QuestionList ql : QuestionList.getTests()){
            // déclaration des variables
            String name = ql.getName();
            Label txt = new Label(name);// le texte
            txt.setFont(modern);
            fenetre.add(txt, 4, i+5,2,1); // ajout du texte
            if(tests.containsKey(name)) { // si le test existe
            Test testPasse = tests.get(ql.getName());
	            if(testPasse.isTerminated()) {// si le test est terminé
	            	Label noteEU;
	            	if(testPasse.isCorrected()) { // et corrigé
	            		noteEU = new Label(String.valueOf(testPasse.getResult()));// on affiche la note
	            	}else{ // si il n'est pas corrigé
	            		noteEU = new Label("Non corrigé");// autre message
	            	}
	            	noteEU.setFont(modern);
	            	fenetre.add(noteEU, 11, i+5,2,1);
	            }
            } else {
            	Button faireLeTest = new Button(">"); // sinon on met un bouton pour passer le test
            	faireLeTest.setOnMouseClicked(e -> {Main.getSceneManager().changeScene(new TestReplyW(Main.getConnectionManager().getCurrentUser(), ql.makeTest(-1)));});
            	faireLeTest.setFont(modern);
            	fenetre.add(faireLeTest, 11, i+5,2,1);
            }
            i++;
        }
		root.setBackground(new Background(this.bg.getBg()));
		root.setTop(header.getPane());
		root.setCenter(fenetre);
		root.setBottom(footer.getPane());
		return root;
	}
}
