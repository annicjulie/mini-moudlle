package windows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import graphics.BackgroundMoodle;
import graphics.Footer;
import graphics.Header;
import graphics.Polices;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import main.Main;
import question.QuestionList;

public class AccueilProfs extends Stage {
	
	Polices police = new Polices();
	private Button newTest = new Button(" + Nouveau test");
	private Button resParEleves = new Button(" Résultats par élèves");
	private Label colonneTest = new Label("- Tests -");
	private Label colonneRes = new Label("- Résultats -");
	private BackgroundMoodle bg = new BackgroundMoodle("/res/background2.png", 1440, 900);
	
	
	/**
	 * Constructeur de la classe
	 * @author rouxsi
	 * @version 1.0
	 */
	public AccueilProfs(){	
		this.setTitle("Acceuil professeurs");// titre de la fenetre
		this.setResizable(Main.RESIZABLE);
		Scene laScene = new Scene(creerContenu(), Main.WIDTH, Main.HEIGHT);
		this.setScene(laScene);
		this.sizeToScene();
	}
	/**
	 * Permet la création du contenu graphique pour la page d'accueil des profils élèves
	 * @author rouxsi
	 * @version 1.0
	 **/
	Parent creerContenu(){
		// le conteneur principal
		
		BorderPane root = new BorderPane();
		
		//le header
		Header header = new Header("Acceuil Professeur");
		
		//le footer
		Footer footer = new Footer();
		
		//le contenu de la fenetre
		GridPane fenetre = new GridPane();
		
		// la police
		Font modern = police.use("modern");
		Font clem = police.use("clem");
		colonneTest.setFont(clem);
		colonneRes.setFont(clem);
		newTest.setFont(modern);
		resParEleves.setFont(modern);
		
		
		//fenetre.setGridLinesVisible(true);
		fenetre.setHgap(50); // taille minimale d'une case
		fenetre.setVgap(10); // taille minimale d'une case
		
		fenetre.add(colonneTest, 3, 4,2,1);// on ajoute le texte -Tests- 
		fenetre.add(colonneRes, 10, 4,2,1); // on ajoute le texte -Résultats-
		
		ArrayList<QuestionList> tests = QuestionList.readFromFS();
		HashMap<QuestionList, Float> moyennes = Main.getTestManager().getAvgOfTests();
		
		int i = 0;
		/**
		 * Permet l'affichage des moyennes pour chaque test
		 * @author leguyaii (rouxsi pour certaines modifs)
		 */
		for(QuestionList ql : tests) {
			System.out.println("question: " + ql.getName());
			Button modifier = new Button("Modifier");
			Label testName = new Label(ql.getName());
			Label result = new Label("Aucune note");
			Button showResults = new Button("Voir résultats");
			for(QuestionList q : moyennes.keySet()) {
				if(q.getName().equals(ql.getName())) {
					float res = moyennes.get(q);
					System.out.println("Moyenne de " + ql.getName() + " : " + res);
					if(res < 0) result.setText("Il reste des tests à corriger");
					else result.setText(String.valueOf(res));
					break;
				}
			}
			
			showResults.setOnAction(e -> {Main.getSceneManager().changeScene(new NoteConsultationW(ql));});
			
            modifier.setOnAction( e -> {
                try{
                    this.accederModif(ql);
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            });
			
			fenetre.add(modifier, 2, i + 5);
			fenetre.add(testName, 4, i + 5, 2, 1);
			fenetre.add(result, 11, i + 5, 2, 1);
			fenetre.add(showResults, 12, i + 5);
			i++;
		}
        
		fenetre.add(newTest, 3, i + 5, 4, 1);
		fenetre.add(resParEleves, 10, i + 5, 4, 1);
		
		newTest.setOnAction(e -> {this.nouvTest();});
		
		root.setBackground(new Background(this.bg.getBg()));
		root.setTop(header.getPane());
		root.setCenter(fenetre);
		root.setBottom(footer.getPane());
		
		return root;
	}
	
	/**
	 * Change la fenetre actuelle en une fenetre de modification du test selectionné
	 * @param ql QuestionList
	 * @author rouxsi
	 * @version 1.0
	 */
	public void accederModif(QuestionList ql){
		Main.getSceneManager().changeScene(new TestModificationW(ql));
	}

	/**
	 * Supprime un test (QuestionList)
	 * @author rouxsi
	 * @param nomFichier le nom du fichier a supprimer
	 * @version 1.0
	 */
	public void suppression(String nomFichier) {
		Alert confirmer = new Alert(AlertType.CONFIRMATION, "Voulez-vous vraiement supprimer cet étudiant ?",ButtonType.CANCEL,ButtonType.OK);
		Optional<ButtonType> option = confirmer.showAndWait();
	      if (option.get() == ButtonType.OK) {
	    	  System.out.println("Fonction pour suprimer");
	      }
	}
	/**
	 * Renvoie sur la page de création de test
	 */
	public void nouvTest(){
		Main.getSceneManager().changeScene(new CreationQuestionnaire());

	}
}
