package windows;

import java.util.ArrayList;
import java.util.Optional;

import graphics.BackgroundMoodle;
import graphics.Header;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.Main;
import question.QCM;
import question.QRT;
import question.QuestionList;

/**
 * Interface de création de questionnaire
 * @author julie
 * @version 1.0
 */

public class CreationQuestionnaire extends Stage {
	
	
	private Button ajout=new Button("Ajouter Question");
	private Button valider=new Button("Valider le questionnaire");
	private static TabPane tabPane = new TabPane();
	private int nbq=1;
	
	private BackgroundMoodle bg;
	private BackgroundMoodle bg2;
	
	private Header head = new Header("MiniMoooodle");
	private TextField nomQ= new TextField("");
	
	
	ArrayList<PageCQ> pages = new ArrayList<PageCQ>();

	
	
	/**
	 * Constructeur CreationQuestionnaire
	 * @version 1.0
	 */
	public CreationQuestionnaire(){
		
		this.setTitle("MiniMoooodle");
		this.setResizable(false);

		this.bg = new BackgroundMoodle("/res/background.png", 1440, 900);
		this.bg2 = new BackgroundMoodle("/res/background2.png", 1440, 900);

		Scene maScene = new Scene(creerContenu());
		this.setScene(maScene);
		this.sizeToScene();
		this.setMinHeight(800);
		this.setMinWidth(1000);
		
		ajout.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>(){
			public void handle(ActionEvent e) {
				nbq++;
				if (nbq<=5){
					PageCQ page = new PageCQ(nbq);
					pages.add(page);
					
					
				}
			}
		});
		
		


	
		
		
		
	}
	
	/**
	 * Méthode qui crée le contenu de la page
	 */
	Parent creerContenu(){
		BorderPane root = new BorderPane();
		
		VBox mid = new VBox();
		mid.setPadding(new Insets(0, 0, 0, 0));
		mid.setSpacing(20);
		
		HBox tete= new HBox();
		tete.setSpacing(20);
		Text nom= new Text("Nom du questionnaire:");
		
		
		this.valider.setOnAction(e -> {validerQ();});
		
		nomQ.setMinWidth(200);
		
		PageCQ pg=new PageCQ(nbq);
		pages.add(pg);

		
		mid.setBackground(new Background(this.bg.getBg()));
		tabPane.setBackground(new Background(this.bg2.getBg()));
	
		
		tete.getChildren().addAll(nom,nomQ,this.ajout);
		mid.getChildren().addAll(tete,this.tabPane,this.valider);
		
		root.setTop(head.getPane());
		root.setCenter(mid);
		
		return root;
	}
	
	
	/**
	 *methode qui permet d'ajouter un onglet(question) à la liste des onglets
	 */
	public static void newtab(Tab t) {
		tabPane.getTabs().add(t);
	}
	

	
	
	
	
	

	
	/**
	 * methode qui valide le questionnaire en créant une questionlist composée de différentes question(max5) ainsi que des réponses si nécessaoire
	 */
	public void validerQ(){
		
		if (this.nomQ.getText().equals("")){
			
			System.out.print("sperlipopette");
			Alert warn = new Alert(AlertType.WARNING, "Veuillez indiquer un titre au questionnaire avant de le valider.",ButtonType.OK);
			Optional<ButtonType> option = warn.showAndWait();
		}else if(intiqvalides()==true){
			Alert warn = new Alert(AlertType.WARNING, "Veuillez indiquer un intitulé pour chacune des questions.",ButtonType.OK);
			Optional<ButtonType> option = warn.showAndWait();
			
			
		}else if(repsvalides()==true){
			Alert warn = new Alert(AlertType.WARNING, "Veuillez indiquer une réponse dans chacun des champs des QCM.",ButtonType.OK);
			Optional<ButtonType> option = warn.showAndWait();
			
			
		}else if(unerepcoche()==true){
			Alert warn = new Alert(AlertType.WARNING, "Veuillez cocher au moins une réponse valide pour chacun des QCM.",ButtonType.OK);
			Optional<ButtonType> option = warn.showAndWait();
		}else{
			String nom=this.nomQ.getText();
			QuestionList questionl = new QuestionList(nom);
			for (int i=0;i<pages.size();i++){
				PageCQ p1 = this.pages.get(i);
				String intitule = p1.getIntiq();
				
				boolean type = p1.getType();
				if (type==true){
					QRT q=new QRT(intitule);
					questionl.addQuestion(q,i);
					System.out.print("txt");
				}else{
					System.out.print("qcm");
					boolean nbres=p1.getnbres();
					
					System.out.println(nbres);
					
					QCM q=new QCM(intitule, nbres, p1.getlesrep());
					questionl.addQuestion(q,i);
				}
			}
			try {
				QuestionList.writeToFS();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			tabPane.getTabs().clear();
			Main.getSceneManager().changeScene(new AccueilProfs());
			
			
			
		}
		
	
		
	}
	
	
	/**
	 * Methode verifiant les intitulés ne sont pas laissé vides.
	 * Appellée avant de valider le questionnaire
	 * @return vrai si un champ est laissé vide
	 */
	public boolean intiqvalides(){
		boolean res= false;
		
		for (int i=0;i<pages.size();i++){
			PageCQ p1 = this.pages.get(i);
			if(p1.getIntiq().equals("")){
				
				res=true;
			}
		}
		
		
		return res;
		
	}
	
	/**
	 * methode verifiant que des champs de réponses ne sont pas laissé vides.
	 * Appellée avant de valider le questionnaire
	 * @return vrai si un champ est laissé vide
	 */
	public boolean repsvalides(){
		boolean res= false;
		
		for (int i=0;i<pages.size();i++){
			
			if (this.pages.get(i).repsvides()==true){
				res=true;
				
			}
		}
		
		
		return res;
		
	}
	
	/**
	 * Methode verifiant qu'il y a au moins une réposnse de coché dans le cas d'un qcm.
	 * Appellée avant de valider le questionnaire
	 * @return vrai si aucune reponse n'est coché dans le cas d'un qcm
	 */
	public boolean unerepcoche(){
		
		boolean res=false;
		for (int i=0;i<pages.size();i++){
			
			if (pages.get(i).getType()==false){
				if(pages.get(i).getnbresv()==true){
					res=true;
				}
			}
			
		}
		
		return res;
		
	}
	
	
}