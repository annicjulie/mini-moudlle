package windows;

import java.util.HashMap;

import graphics.BackgroundMoodle;
import graphics.Footer;
import graphics.Header;
import graphics.Polices;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import main.Main;
import managers.ConnectionManager;
import question.QuestionList;
import question.Test;
import users.Utilisateur;

/**
 * Classe de la fenêtre de consultation des notes.
 * @author leguyaai
 * @version 1.0
 */

public class NoteConsultationW extends Stage {

	private QuestionList questList;
	private Polices police;
	private BackgroundMoodle bg = new BackgroundMoodle("/res/background2.png", 1440, 900);
	private HashMap<Button, String> UserByButton;
	
	/**
	 * C'est le constructeur de la classe NoteConsultationW. Il prend en paramètre un test.
	 * @since 1.0
	 * @param ql QuestionList
	 */
	
	public NoteConsultationW(QuestionList ql) {
		this.setTitle("Consultation des notes du test \"" + ql.getName() + "\"");
		this.setWidth(Main.WIDTH);
		this.setHeight(Main.HEIGHT);
		this.setResizable(Main.RESIZABLE);
		this.questList = ql;
		this.police = new Polices();
		this.UserByButton = new HashMap<Button, String>();
		Scene sc = new Scene(this.display());
		this.setScene(sc);
	}
	
	/**
	 * Cette méthode va être appelée par le constructeur pour initialiser la fenêtre.
	 * C'est elle qui va placer et gérer tout les éléments de la fenêtre.
	 * @since 1.0
	 * @return BorderPane root
	 */
	
	private Parent display() {
		BorderPane root = new BorderPane();
		ScrollPane sp = new ScrollPane();
		Header header = new Header(this.getTitle());
		Footer footer = new Footer();
		VBox main = setupTests();
		main.setSpacing(15);
		main.setAlignment(Pos.CENTER);
		sp.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		sp.setFitToWidth(true);
		sp.setContent(main);
		root.setCenter(sp);
		root.setBottom(footer.getPane());
		root.setTop(header.getPane());
		BorderPane.setMargin(sp, new Insets(150, 350, 150, 350));
		root.setBackground(new Background(this.bg.getBg()));
		return root;
	}

	/**
	 * Cette méthode met en place l'affichage des résultats des tests par élèves.
	 * @since 1.0
	 * @return VBox vbox
	 */
	
	private VBox setupTests() {
		VBox vbox = new VBox();
		Font modern = this.police.use("modern");
		String[] users = ConnectionManager.getUsers();
		for (int i = 0; i < users.length; i++) {
			Label label = new Label();
			label.setFont(modern);
			HashMap<String, Test> hm = Utilisateur.getTestsForUser(users[i]);
			if (hm.get(this.questList.getName()) != null) { 
				if (!(hm.get(this.questList.getName()).getResult() < 0)) {
					label.setTextFill(Color.GREEN);
					label.setText(users[i] + " a eu " + hm.get(this.questList.getName()).getResult() + " au test " + this.questList.getName());
					vbox.getChildren().addAll(label);
				}
				else {
					HBox hb = new HBox();
					Button txt = new Button("TXT");
					label.setTextFill(Color.ORANGE);
					label.setText("Le test de " + users[i] + " n'a pas encore été corrigé");
					hb.getChildren().addAll(label, txt);
					hb.setSpacing(10);
					vbox.getChildren().addAll(hb);
					hb.setAlignment(Pos.CENTER);
					this.UserByButton.put(txt, users[i]);
					txt.setOnAction(e -> {openValRepTextu(txt);});
				}
			}
			else {
				label.setTextFill(Color.RED);
				label.setText(users[i] + " n'a pas encore passé le test.");
				vbox.getChildren().addAll(label);
			}
		}
		return vbox;
	}
	
	private void openValRepTextu(Button btn) {
		Main.getSceneManager().changeScene(new ValRepTextu(this.questList, Utilisateur.getByLogName(this.UserByButton.get(btn))));
	}
	
}
