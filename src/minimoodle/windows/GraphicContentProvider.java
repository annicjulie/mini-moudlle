package windows;

import javafx.scene.Group;

/**
 * Permet a une classe de produire un contenu a afficher, sans creer de Stage JavaFX
 * @author lerouzyi
 *
 */
public interface GraphicContentProvider {
	
	/**
	 * 
	 * @return un groupe d'elements JavaFx qui pourra etre ajoute a un Stage
	 */
	public Group getGraphicContent();
}
