package graphics;

import javafx.scene.text.Font;

/**
 * Permet l'ajout de polices
 * @author rouxsi
 * @version 1.0
 **/
public class Polices {
	
	private Font CLEM;
	private Font MODERN;

	/**
	 * Permet d'affecter une police 
	 * @author rouxsi
	 * @version 1.0
	 **/
	public Polices() {
		try {
			this.CLEM = Font.loadFont(this.getClass().getResourceAsStream("/res/polices/ClementePDae-Light.ttf"), 50);
			this.MODERN = Font.loadFont(this.getClass().getResourceAsStream("/res/polices/ModernSans-Light.otf"), 20);
		} catch(Exception e) {e.printStackTrace();}
	}
	/**
	 * Permet d'affecter une police 
	 * @author rouxsi
	 * @version 1.0
	 * @param s une chaine de caractères contenant le nom de la police
	 * @return la police correspondante ou rien si le nom de correspond à aucune constante
	 **/
	public Font use(String s){
		Font res;
		if(s=="clem"){
			res=CLEM;
		}else if(s=="modern"){
			res=MODERN;
		}else{
			res=null;
		}
		return res;
	}
}
