package graphics;

import javafx.scene.image.Image;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

/**
 * Classe BackgroundMoodle, elle s'occupe de pouvoir générer un background JavaFX à partir d'une image
 * @author leguyaai
 * @version 1.0
 * 
 */


public class BackgroundMoodle {

	private BackgroundImage bi;
	
	/**
	 * Constructeur du background qui créer le background de JavaFX à partir d'une image et de ses propriétés.
	 * @since 1.0
	 * @param way String, chemin de l'image
	 * @param width double, Longueur de l'image
	 * @param height double, Hauteur de l'image
	 */
	
	public BackgroundMoodle(String way, double width, double height) {
		this.bi = new BackgroundImage(new Image(this.getClass().getResourceAsStream(way),width,height,false,true), BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				new BackgroundSize(width, height, true, true, true, true));
	}

	/**
	 * Cette méthode permet de récupérer le background généré par cette classe.
	 * @since 1.0
	 * @return bi BackgroundImage
	 */
	
	public BackgroundImage getBg() {
		return this.bi;
	}
	
}

