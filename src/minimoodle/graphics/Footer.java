package graphics;

import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Classe Footer, elle génère le footer des différentes pages (C'est un élément purement graphique).
 * @author leguyaai
 * @version 1.0
 * 
 */


public class Footer {

	private Rectangle rc;
	private StackPane sp;
	
	/**
	 * Constructeur de la classe Footer qui génère le footer.
	 * @since 1.0
	 */
	
	public Footer() {
		this.rc = createRectangle(this.rc);
		this.sp = init();
	}
	
	
	/**
	 * Cette méthode génère un rectangle qui est utilisé dans le footer.
	 * @param r Rectangle
	 * @return r Rectangle
	 */
	private Rectangle createRectangle(Rectangle r) {
		r = new Rectangle(1280, 60);
		r.maxWidth(Double.MAX_VALUE);
		r.prefWidth(1280);
		r.setFill(Color.grayRgb(50, 1));
		return r;
	}
	
	/**
	 * Cette méthode est appelée dans le constructeur pour positionner les éléments.
	 * @since 1.0
	 */
	
	private StackPane init() {
		StackPane sp = new StackPane();
		sp.getChildren().addAll(this.rc);
		sp.setAlignment(Pos.CENTER_LEFT);
		return sp;
	}
	
	/**
	 * Méthode pour récupérer le pane du Footer pour pouvoir l'ajouter dans un groupe.
	 * @since 1.0
	 * @return sp StackPane
	 */
	
	public StackPane getPane() {
		return this.sp;
	}
}
