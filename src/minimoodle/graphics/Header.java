package graphics;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import main.Main;
import windows.LogPage;

/**
 * Classe Header, elle génère le header de chaque fenêtre
 * @author leguyaai
 * @version 1.0
 * 
 */


public class Header {

	private String title;
	private Rectangle r;
	private ImageView disconnect;
	private ImageView logo;
	private ImageView home;
	private FlowPane content;
	private StackPane sp;
	
	private static String pathDisco = "/res/decolog_2.png";
	private static String pathStudentHome = "/res/accueil_eleve.png";
	private static String pathProfesorHome = "/res/accueil_prof.png";
	
	/**
	 * Constructeur de la classe Header
	 * @since 1.0
	 * @param title String
	 */
	
	public Header(String title) {
		this.title = title;
		this.r = createRectangle(this.r);
		this.logo = createLogo(new ImageView());
		
		this.content = new FlowPane();
		this.content.setAlignment(Pos.CENTER_LEFT);
		Label np = new Label(Main.getConnectionManager().getCurrentUser().getPrenom() + " " + Main.getConnectionManager().getCurrentUser().getNom().toUpperCase());
		np.setFont(new Font(24));
		np.setTextFill(Color.GRAY);
		Text sep = new Text("  |  ");
		np.setFont(new Font(24));
		sep.setFill(Color.LIME);
		Text ht = new Text(this.title);
		ht.setFont(new Font(24));
		ht.setFill(Color.BLACK);
		
		this.content.getChildren().add(np);
		this.content.getChildren().add(sep);
		this.content.getChildren().add(ht);
		this.disconnect = createDisconnect(new ImageView());
		this.disconnect.setOnMouseClicked(e -> {Exit();});
		this.home = createHome(new ImageView());
		this.home.setOnMouseClicked(e -> {Main.getSceneManager().changeScene(Main.getConnectionManager().getAccueil());});
		this.sp = init();
	}
	
	/**
	 * Méthode appelée par le contructeur pour mettre en place les éléments contenus dans le Header
	 * @since 1.0
	 * @return sp StackPane
	 */
	
	private StackPane init() {
		StackPane sp = new StackPane();
		sp.getChildren().addAll(this.r, this.logo, this.content, this.home, this.disconnect);
		sp.setAlignment(Pos.CENTER_LEFT);
		StackPane.setMargin(this.logo, new Insets(0, 0, 0, 10));
		StackPane.setMargin(this.content, new Insets(0, 0, 0, 100));
		StackPane.setMargin(this.disconnect, new Insets(0, 10, 0, 1200));
		StackPane.setMargin(this.home, new Insets(0, 0, 0, 1130));
		return sp;
	}
	
	/**
	 * Méthode pour récupérer le pane du Header pour pouvoir l'ajouter dans un groupe.
	 * @since 1.0
	 * @return sp StackPane
	 */
	
	public StackPane getPane() {
		return this.sp;
	}
	
	/**
	 * Méthode pour initialiser un rectangle qui est un élément du Header.
	 * @since 1.0
	 * @param r Rectangle
	 * @return r Rectangle
	 */
	
	private Rectangle createRectangle(Rectangle r) {
		r = new Rectangle(1280, 80);
		r.maxWidth(Double.MAX_VALUE);
		r.prefWidth(1280);
		r.setFill(Color.LIGHTGRAY);
		return r;
	}
	
	/**
	 * Méthode pour créer l'élément qui permet de se déconnecter.
	 * @since 1.0
	 * @param disco ImageView
	 * @return disco ImageView
	 */
	
	private ImageView createDisconnect(ImageView disco) {
		Image img = new Image(this.getClass().getResourceAsStream(pathDisco), 676, 676, false, true);
		disco.setImage(img);
		disco.setFitHeight(70);
		disco.setFitWidth(70);
		return disco;
	}
	
	/**
	 * Méthode pour créer l'élément qui permet à l'utilisateur de revenir à l'accueil.
	 * @since 1.0
	 * @param c Circle
	 * @return c Circle
	 */
	
	private ImageView createHome(ImageView home) {
		Image img = new Image(this.getClass().getResourceAsStream(pathStudentHome), 676, 676, false, true);
		if (Main.getConnectionManager().getCurrentUser().isProf()) {
			img = new Image(this.getClass().getResourceAsStream(pathProfesorHome), 676, 676, false, true);
		}
		home.setImage(img);
		home.setFitHeight(70);
		home.setFitWidth(70);
		return home;
	}
	
	/**
	 * Cette méthode permet de créer le logo de notre application.
	 * @since 1.0
	 * @param imgV ImageView
	 * @return imgV ImageView
	 */
	
	private ImageView createLogo(ImageView imgV) {
		try {
			Image img = new Image(this.getClass().getResourceAsStream("/res/logoMinimoo.png"));
			imgV.setImage(img);
			imgV.setFitHeight(70);
			imgV.setFitWidth(70);
		}
		catch (Exception e) {e.printStackTrace();}
		return imgV;
	}
	
	/**
	 * Cette méthode permet de gérer l'action de click sur le cercle exit lorsque celui ci est clické
	 * @since 1.0
	 **/
	private void Exit() {
		try {
			Main.getConnectionManager().disconnect();
			Main.getSceneManager().changeScene(new LogPage());
		} catch (Exception ex) {
			ex.printStackTrace();
			}
		}

}
