package users;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import main.Main;
import question.QRT;
import question.Question;
import question.Test;

/**
 * @author lerouzyi 
 */

@SuppressWarnings("serial")
public class Utilisateur implements Serializable {
	
	public static final String USERS_DIR = "data/users/";
	public static final String TESTS_FILE = "answered_tests.dat";

	private String nom, prenom, logname, password;
	private boolean prof;
	private transient HashMap<String, Test> tests;
	
	public Utilisateur(String n, String p, String pwd, boolean prof) {
		this.nom = n;
		this.prenom = p;
		this.prof = prof;
		this.setPassword(pwd);
		this.tests = new HashMap<String, Test>();
		this.logname = (this.prenom.charAt(0) + this.nom).toLowerCase().replace(' ', '_');
	}
	
	/**
	 * serialise l'utilisateur dans un fichier user.dat, dans un dossier nommé comme son nom de login
	 */
	public void writeToFS() {
		File dir = new File(USERS_DIR + this.getLogname() + "/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File(USERS_DIR + this.getLogname() + "/user.dat"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
            fos = new FileOutputStream(new File(USERS_DIR + this.getLogname() + "/" + TESTS_FILE));
            oos = new ObjectOutputStream(fos);
            oos.writeObject(this.tests);
            oos.close();
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture de l'utilisateur " + this.getLogname());
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param username l'identifiant de l'utilisateur
	 * @param password le mot de passe de l'utilisateur
	 * @return l'utilisateur si la tentative de connexion aboutit (username existant et bon password), null sinon
	 */
	public static Utilisateur login(String username, String password) {
		File in = new File(USERS_DIR + username + "/user.dat");
		if(in.exists()) {
			try {
				FileInputStream fis = new FileInputStream(in);
				ObjectInputStream ois = new ObjectInputStream(fis);
				Utilisateur attempt = (Utilisateur) ois.readObject();
				ois.close();
				fis.close();
				attempt.tests = Utilisateur.getTestsForUser(username);
				if(attempt.getPassword().equals(password)) return attempt;
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public static Utilisateur getByLogName(String logname) {
		File in = new File(USERS_DIR + logname + "/user.dat");
		if(in.exists()) {
			try {
				FileInputStream fis = new FileInputStream(in);
				ObjectInputStream ois = new ObjectInputStream(fis);
				Utilisateur attempt = (Utilisateur) ois.readObject();
				ois.close();
				fis.close();
				attempt.tests = Utilisateur.getTestsForUser(logname);
				return attempt;
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param username l'identifiant de l'utilisateur
	 * @return une hmap des tests qu'a passe l'utilisateur, dont la cle est le nom du test
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, Test> getTestsForUser(String username) {
		HashMap<String, Test> ret = null;
		File in = new File(USERS_DIR + username + "/" + TESTS_FILE);
		if(in.exists()) {
			try {
				FileInputStream fis = new FileInputStream(in);
				ObjectInputStream ois = new ObjectInputStream(fis);
				ret = (HashMap<String, Test>) ois.readObject();
				ois.close();
				//Main.logDebug("L'utilisateur " + username + " a pass� " + ret.size() + " tests: ");
				for(String name : ret.keySet()) {
					Main.logDebug(name + ", note : " + ret.get(name).getResult());
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	/**
	 * 
	 * @param test le nom du test concerne
	 * @return ArrayList de toutes les QRT du test
	 */
	public ArrayList<QRT> getUncorrectedQuestionsForTest(String test) {
		ArrayList<QRT> questions = new ArrayList<QRT>();
		
		if(this.tests.containsKey(test)) {
			for(Question q : this.tests.get(test).getQuestions()) {
				if(q.getType().equals("QRT") && q.getScore() == Question.NOT_CORRECTED) {
					questions.add((QRT) q);
				}
			}
		}
		
		return questions;
	}
	
	/**
	 * ajoute un test a l'utilisateur (qu'il soit 
	 * @param t le test a ajouter
	 */
	public void addTest(Test t) {
		this.tests.put(t.getName(), t);
	}
	
	public Test getTest(String key) {
		return this.tests.get(key);
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getLogname() {
		return logname;
	}

	public void setLogname(String logname) {
		this.logname = logname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isProf() {
		return prof;
	}

	public void setProf(boolean prof) {
		this.prof = prof;
	}
}
