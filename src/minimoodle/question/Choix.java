package question;

import java.io.Serializable;

import main.Main;

/**
 * 
 * @author lerouzyi
 *
 */

@SuppressWarnings("serial")
public class Choix implements Serializable {

	private String intitule;
	private boolean chosen, correctState;
	
	/**
	 * 
	 * @param i intitule du choix
	 * @param c validite du choix (doit-il etre coche ?) 
	 */
	public Choix(String i, boolean c) {
		this.intitule = i;
		this.correctState = c;
	}
	
	public void toggle() {
		this.chosen = !this.chosen;
	}

	/**
	 * 
	 * @return le score (-1, 0, 1) obtenu pour ce choix
	 */
	public int getScore() {
		int ret = -1;
		if(this.correctState == false && this.chosen == false) {
			Main.logDebug(this.getIntitule() + ": ne doit pas etre coche et n'est pas coche: 0");
			ret = 0;
		}
		else if(this.correctState == true && this.chosen == true) {
			Main.logDebug(this.getIntitule() + ": doit etre coche et est coche: 1");
			ret = 1;
		} else {
			Main.logDebug(this.getIntitule() + ": est incorrect: -1");
		}
		
		return ret;
	}

	/*
	 * intitule du choix
	 */
	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	/*
	 * etat actuel du choix: coche (true), ou non (falseà
	 */
	public boolean isChosen() {
		return chosen;
	}

	public void setChosen(boolean chosen) {
		this.chosen = chosen;
	}

	/*
	 * etat de la variable chosen pour que la reponse soit valide
	 */
	public boolean getCorrectState() {
		return correctState;
	}

	public void setCorrectState(boolean correctState) {
		this.correctState = correctState;
	}
	
	public String toString() {
		String s = this.getIntitule();
		if(this.correctState) s += ": doit etre choisi, ";
		else s += ": ne doit pas etre choisi";
		
		if(this.chosen) s+= "est choisi";
		else s += "n'est pas choisi";
		
		return s;
	}
}
