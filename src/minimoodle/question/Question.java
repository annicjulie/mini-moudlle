package question;

import java.io.Serializable;

import windows.GraphicContentProvider;

/**
 * 
 * @author lerouzyi
 *
 */

@SuppressWarnings("serial")
public abstract class Question implements GraphicContentProvider, Serializable {
	
	public static final int NOT_CORRECTED = -100;
	
	private String intitule;
	protected int score = NOT_CORRECTED;
	
	public Question(String i) {
		this.intitule = i;
	}
	
	public abstract String toString();
	
	/**
	 * 
	 * @return la note qu'a obtenu l'eleve au questionnaire
	 */
	public int getScore() {
		return this.score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	/**
	 * 
	 * @return le type de question (QCM pour Question a Choix Multiples, ou QRT pour Question a Reponse Textuelle)
	 */
	public abstract String getType();
	
	/**
	 * 
	 * @return l'intitule de la question
	 */
	public String getIntitule() {
		return this.intitule;
	}
	
	public void setIntitule(String i) {
		this.intitule = i;
	}
}
