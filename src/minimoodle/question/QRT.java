package question;

import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

/**
 * QRT - Question a Reponse Textuelle
 * @author lerouzyi
 *
 */

@SuppressWarnings("serial")
public class QRT extends Question {

	private String reponse;
	private transient TextArea tf;
	
	/**
	 * 
	 * @param intitule intitule de la question
	 */
	public QRT(String intitule) {
		super(intitule);
		this.setScore(Question.NOT_CORRECTED);
	}

	/**
	 * 
	 * @return reponse entree par l'utilisateur
	 */
	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	public String toString() {
		return "Intitule: " + this.getIntitule() + "\nReponse: " + this.getReponse();
	}
	
	public void saveReponse() {
		if(this.tf != null) this.setReponse(tf.getText());
	}
	
	public int getScore() {
		return this.score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}

	public Group getGraphicContent() {
		Group g = new Group();
		
		Label intitule = new Label(this.getIntitule());
		VBox content = new VBox();
		content.getChildren().add(intitule);
		
		this.tf = new TextArea();
		this.tf.setPrefHeight(128);
		this.tf.setPrefWidth(640);
		content.getChildren().add(tf);
		
		g.getChildren().add(content);
		
		return g;
	}
	
	public String getType() {
		return "QRT";
	}
}
