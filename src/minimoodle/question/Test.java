package question;

import java.io.Serializable;
import java.util.ArrayList;

import javafx.scene.Group;
import javafx.scene.control.Label;
import windows.GraphicContentProvider;

/**
 * objet Test: lot de questions posees a l'utilisateur
 * @author lerouzyi
 *
 */

@SuppressWarnings("serial")
public class Test implements GraphicContentProvider, Serializable {

	private static ArrayList<Test> testsList = new ArrayList<Test>();
	private ArrayList<Question> questions;
	private int currentQuestion;
	private boolean isCorrected;
	private String name;

	/**
	 * 
	 * @param qs arraylist du lot de questions qui seront posees a l'utilisateur
	 * @param name le nom du test (qui est affiche dans le header)
	 */
	public Test(String name, ArrayList<Question> qs) {
		this.questions = qs;
		this.currentQuestion = -1;
		this.name = name;
		testsList.add(this);
	}
	
	/**
	 * passe a la question suivante
	 */
	public void nextQuestion() {
		if(this.currentQuestion >= 0 && this.questions.get(this.currentQuestion) instanceof QRT) {
			QRT qrt = (QRT) this.questions.get(this.currentQuestion);
			qrt.saveReponse();
		}
		this.currentQuestion++;
	}
	
	/**
	 * 
	 * @return true si le test est termine (toutes les questions ont ete repondues), false sinon
	 */
	public boolean isTerminated() {
		return this.currentQuestion >= this.questions.size();
	}
	
	/**
	 * 
	 * @return la note obtenue au test, sur 20
	 */
	public int getResult() {
		int ret = 0;
		boolean hasUncorrected = false;
		for(Question q : this.questions) {
			if(q.getScore() == Question.NOT_CORRECTED) hasUncorrected = true;
			else if(q.getScore() >= 0) ret += q.getScore();
		}
		if(hasUncorrected) ret = Question.NOT_CORRECTED;
		else {
			if(ret < 0) ret = 0;
			float r = ((ret * 1.0f) / this.questions.size()) * 20.0f;
			ret = (int) r;
			this.isCorrected = true;
		}
		return ret;
	}
	
	/**
	 * 
	 * @return true si le test est corrige (donc si le prof a valide et attribue une note aux reponses textuelles), false sinon
	 */
	public boolean isCorrected() {
		if(!this.isCorrected) this.isCorrected = this.getResult() != Question.NOT_CORRECTED;
		return this.isCorrected;
	}
	
	public String toString() {
		return this.questions.get(this.currentQuestion).toString();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Group getGraphicContent() {
		if(!this.isTerminated()) return this.questions.get(this.currentQuestion).getGraphicContent();
		else {
			Group g = new Group();
			Label l = new Label("Le test est terminé, merci. <o/");
			g.getChildren().add(l);
			return g;
		}
	}
	
	/**
	 * cree un test depuis une QuestionList (appelle la methode makeTest de QuestionList)
	 * @param ql la liste de questions source
	 * @param nbQuestions le nombre de questions a inclure
	 * @return un test de nbQuestions questions
	 */
	public static Test fromQuestionList(QuestionList ql, int nbQuestions) {
		return ql.makeTest(nbQuestions);
	}
	
	/**
	 * 
	 * @return tous les tests que le programme a cree
	 */
	public static ArrayList<Test> getTestList() {
		return testsList;
	}

	/**
	 * 
	 * @return toutes les questions du test
	 */
	public ArrayList<Question> getQuestions() {
		return questions;
	}
}
