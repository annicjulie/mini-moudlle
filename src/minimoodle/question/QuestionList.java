package question;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * liste de question: une liste de toutes les questions qui peuvent apparaitre dans un test, chargee depuis un fichier
 * @author lerouzyi
 * 
 */

@SuppressWarnings("serial")
public class QuestionList implements Serializable {

	private static final String QL_DATADIR = "data/lists/";
	
	private ArrayList<Question> questions;
	private String name;
	private static ArrayList<QuestionList> listeQuestions = readFromFS();
	
	public QuestionList(String name) {
		this.name = name;
		this.questions = new ArrayList<Question>();
		listeQuestions.add(this);
		try {
			writeToFS();
		} catch (Exception e) {e.printStackTrace();}
	}
	
	public static void setQuestionInList(int index, QuestionList ql) {
		listeQuestions.set(index, ql);
	}
	
	public void setQuestions(ArrayList<Question> qs) {
		this.questions = qs;
	}
	
	public void addQuestion(Question q, int ind) {
		this.questions.add(ind, q);
	}
	
	public void removeQuestion(int index) {
		this.questions.remove(index);
	}
	
	public ArrayList<Question> getQuestions() {
		return this.questions;
	}
	
	public static ArrayList<QuestionList> getTests() {
		ArrayList<QuestionList> list = null;
		try {
			list = readFromFS();
		} catch (Exception e) {e.printStackTrace();}
		return list;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionList other = (QuestionList) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * genere un objet test a partir de la liste de questions
	 * @param nbQuestions le nombre de questions qui seront piochees aleatoirement dans cette liste pour apparaitre dans le test genere
	 * @return le test genere, contenant nbQuestions questions
	 */
	public Test makeTest(int nbQuestions) {
		if(nbQuestions == -1) nbQuestions = this.questions.size();
		if(nbQuestions <= this.questions.size()) {
			ArrayList<Question> out = new ArrayList<Question>(nbQuestions);
			
			//sort -R sur l'array list
			Collections.shuffle(this.questions);
			
			for(int i = 0; i < nbQuestions; i++) {
				out.add(this.questions.get(i));
			}
			
			return new Test(this.getName(), out);
		} else {
			System.err.println("Pas assez de questions dans la liste");
			return null;
		}
	}
	
	public int size() {
		return this.questions.size();
	}
	
	public String toString() {
		String s = "";
		for(Question q : this.questions) s += q.toString() + "\n";
		return s;
	}
	
	public String getName() {
		return this.name;
	}
	
	public static String getFileName(String name) {
		return QL_DATADIR + name.toLowerCase().replace(' ', '_') + ".dat";
	}
	
	/**
	 * ecrit la QuestionList dans QL_DATADIR/liste_questions.dat
	 * @throws Exception Exception
	 */
	public static void writeToFS() throws Exception {
		File f = new File(QL_DATADIR);
		if(!f.exists()) f.mkdirs();
        FileOutputStream fos = new FileOutputStream(new File(QL_DATADIR + "liste_questions.dat"));
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(listeQuestions);
        oos.close();
	}
	
	/**
	 * charge une QuestionList depuis un fichier
	 * @return ArrayList de toutes les QuestionList du programme
	 * @throws Exception Exception
	 */
	
	@SuppressWarnings("unchecked")
	public static ArrayList<QuestionList> readFromFS() {
		ArrayList<QuestionList> ret = null;
		try {
	    	FileInputStream fis = new FileInputStream(QL_DATADIR + "liste_questions.dat");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (ArrayList<QuestionList>) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Generating new ArrayList for questions");
			ret = new ArrayList<QuestionList>();
		}
		return ret;
	}
}
