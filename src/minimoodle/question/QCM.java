package question;

import java.util.ArrayList;

import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

/**
 * QCM - Question a Choix Multiples
 * @author lerouzyi
 *
 */

@SuppressWarnings("serial")
public class QCM extends Question {
	
	/**
	 * separateur de champs pour la serialisation
	 */
	public static final char REPS_SEP = '#';
	
	private ArrayList<Choix> choix;
	private boolean isXor;
	
	/**
	 * 
	 * @param intitule intitule du QCM
	 * @param xor le choix est-il exclusif (donc boutons radio), ou non (cases a cocher)
	 * @param chx ArrayList de choix
	 */
	public QCM(String intitule, boolean xor, ArrayList<Choix> chx) {
		super(intitule);
		this.choix = chx;
		this.isXor = xor;
	}
	
	/**
	 * 
	 * @return la note obtenue a la question
	 */
	public int getScore() {
		int ret = 0;
		for(Choix c : this.choix) ret += c.getScore();
		return ret;
	}
	
	public ArrayList<Choix> getChoix() {
		return this.choix;
	}
	
	public boolean isXor() {
		return this.isXor;
	}
	
	/**
	 * choisit un choix, et si besoin, modifie les autres choix
	 * @param n le choix a choisir
	 */
	public void pickChoice(int n) {
		if(this.isXor) {
			for(Choix c : this.choix) c.setChosen(false);
			this.choix.get(n).setChosen(true);
		} else {
			this.choix.get(n).toggle();
		}
	}

	public String toString() {
		String s = this.getIntitule();
		for(Choix c : this.choix) s += "\n* " + c.toString();
		return s;
	}
	
	public Group getGraphicContent() {
		Group g = new Group();
		
		Label intitule = new Label(this.getIntitule());
		VBox content = new VBox();
		content.getChildren().add(intitule);
		
		if(this.isXor) {
			ToggleGroup toggles = new ToggleGroup();
			for(Choix c : this.choix) {
				RadioButton rb = new RadioButton(c.getIntitule());
				rb.setOnMouseClicked(e -> {this.pickChoice(this.choix.indexOf(c));});
				toggles.getToggles().add(rb);
				content.getChildren().add(rb);
			}
		} else {
			for(Choix c : this.choix) {
				CheckBox cb = new CheckBox(c.getIntitule());
				cb.setOnMouseClicked(e -> {this.pickChoice(this.choix.indexOf(c));});
				content.getChildren().add(cb);
			}
		}
		
		g.getChildren().add(content);
		
		return g;
	}
	
	public String getType() {
		return "QCM";
	}
}
