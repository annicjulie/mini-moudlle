package managers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author leguyaai
 * @version 1.0
 * @deprecated Serialization
 */

public class Fichier {

	private File f;
	
	/**
	 * Constructeur de la classe fichier, il permet d'initialiser un ficheir à partir d'une chaine de caractères.
	 * @param name String
	 * @since 1.0
	 */
	
	public Fichier(String name) {
		this.f = new File(name);
	}
	
	/**
	 * Cette méthode permet d'écrire dans un fichier.
	 * @param label String
	 * @since 1.0
	 * @throws Exception Exception
	 */
	
	public void write(String label) throws Exception {
		FileWriter fw = new FileWriter(this.f);
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write(label);
		bw.close();
	}
	
	/**
	 * Cette méthode permet de lire dans un fichier.
	 * @since 1.0
	 * @return contenu String : Le contenu de fichier avec sa forme toString()
	 */
	
	public String read() {
		StringBuffer contenu = new StringBuffer("");
		FileInputStream ftemp = null;
	    int car;
	    
	   try {
	        ftemp = new FileInputStream(f);
	        while( (car = ftemp.read()) != -1) contenu.append((char) car);
	        ftemp.close();
	      }
	      
	   catch(FileNotFoundException e) {
	        System.out.println("Fichier introuvable");
	      }
	    
	   catch(IOException ioe) {
	        System.out.println("Exception " + ioe);
	      }
		
		return contenu.toString();
	}
	
	/**
	 * Cette méthode permet d'afficher le cotnenu du fichier.
	 * @since 1.0
	 */
	
	public void print() {
		System.out.println(this.read());
	}
	
}
