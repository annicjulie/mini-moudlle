package managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import question.QuestionList;
import question.Test;
import users.Utilisateur;

@SuppressWarnings("serial")

/**
 * Classe TestManager, c'est avec cette classe que l'on gère les d'informations des tests.
 * @author leguyaai
 * @version 1.0
 */


public class TestManager implements Serializable {

	private transient static final String AVG_TESTS_BY_STUDENTS_DATADIR = "data/avg_tests_by_students/";
	private transient static final String FILE_NAME = "avgPerTests.dat";
	private HashMap<QuestionList, Float> avgTestsByStudents;
	
	
	/**
	 * C'est le constructeur de la classe TestManager. Il initialise l'HashMap des moyennes des utilisateurs.
	 * @since 1.0 
	 */
	
	public TestManager() {
		this.avgTestsByStudents = getAvgFromEachQuestionList();
		/*try {
			writeToFS();
		} catch (Exception e) {e.printStackTrace();}*/
	}
	
	/**
	 * Cette méthode renvoie une HashMap avec en entree un Test, qui renvoie la moyenne de celui-ci par rapport à tout les utilisateurs.
	 * @since 1.0
	 * @return HashMap<QuestionList, Float> avgQl
	 * @throws  
	 */
	
	private HashMap<QuestionList, Float> getAvgFromEachQuestionList()  {
		HashMap<QuestionList, Float> avgQl = new HashMap<QuestionList, Float>();
		HashMap<QuestionList, Integer> totNoteByQuestionList = new HashMap<QuestionList, Integer>();
		HashMap<QuestionList, Integer> userWhoHavePassedTest = new HashMap<QuestionList, Integer>();
		HashMap<QuestionList, Test> QuestionListAndTest = new HashMap<QuestionList, Test>();
		ArrayList<QuestionList> listQuestionList = QuestionList.readFromFS();
		//On initialise les 2 hashmap avec 0 comme valeur
		for (QuestionList ql2 : listQuestionList) {
			totNoteByQuestionList.put(ql2, 0);
			userWhoHavePassedTest.put(ql2, 0);
		}
		//Tableau contenant les identifiants de tous les utilisateurs
		String[] users = ConnectionManager.getUsers();
		//Pour chaque utilisateur
		for (int i = 0; i < users.length; i++) {
			//On initialise la hashmap avec la m�thode getTestsForUser de la classe Utilisateur
			HashMap<String, Test> tests = Utilisateur.getTestsForUser(users[i]);
			for (String s : tests.keySet()) {
				System.out.println("Tests passés par "  + users[i] + " : " + tests.get(s).getName());
			}
			for (QuestionList questList : listQuestionList) {
				Test testOfUser = tests.get(questList.getName());
				if (testOfUser == null) {
					continue;
				}
				QuestionListAndTest.put(questList, testOfUser);
				if (!testOfUser.isCorrected()) {
					System.out.println("Le test " + testOfUser.getName() + " n'a pas été corrigé");
					totNoteByQuestionList.put(questList, testOfUser.getResult());
					userWhoHavePassedTest.put(questList, 1);
				}
				else {
					totNoteByQuestionList.put(questList, totNoteByQuestionList.get(questList) + testOfUser.getResult());
					userWhoHavePassedTest.put(questList, userWhoHavePassedTest.get(questList) + 1);
				}
			}
		}
		for (QuestionList q : listQuestionList) {
			if (QuestionListAndTest.get(q) != null) {
				System.out.println("Utilisateurs qui ont passé le test " + q.getName() + " : " + userWhoHavePassedTest.get(q));
				avgQl.put(q, new Float(totNoteByQuestionList.get(q)) / userWhoHavePassedTest.get(q));
				System.out.println("Valeur : " + totNoteByQuestionList.get(q));
				System.out.println("Moyenne : " + avgQl.get(q));
			}
		}
		return avgQl;
	}
	
	/**
	 * Cette méthode renvoie l'HashMap calculée par la méthode getAvgFromEachQuestionList()
	 * @since 1.0
	 * @return HashMapQuestionList, Float avgTestsByStudents
	 */
	
	public HashMap<QuestionList, Float> getAvgOfTests() {
		return this.avgTestsByStudents;
	}
	
	/**
	 * Cette méthode permet d'actualiser le fichier contenant les moyennes des tests.
	 * @since 1.0
	 */
	
	public void refresh() {
		this.avgTestsByStudents = getAvgFromEachQuestionList();
		try {
			this.writeToFS();
		} catch(Exception e) {e.printStackTrace();}
	}
	
	/**
	 * Cette méthode permet d'écrire l'objet TestManager dans un fichier.
	 * @since 1.0
	 * @throws Exception Exception
	 */
	
	public void writeToFS() throws Exception {
		File f = new File(AVG_TESTS_BY_STUDENTS_DATADIR);
		if(!f.exists()) f.mkdirs();
        FileOutputStream fos = new FileOutputStream(new File(AVG_TESTS_BY_STUDENTS_DATADIR + FILE_NAME));
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        oos.close();
	}
	
	/**
	 * Cette méthode permet de récupérer un TestManager depuis un fichier.
	 * @since 1.0
	 * @return TestManager ret
	 */
	
	public static TestManager readFromFS() {
		TestManager ret = null;
		try {
			FileInputStream fis = new FileInputStream(AVG_TESTS_BY_STUDENTS_DATADIR + FILE_NAME);
			ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (TestManager) ois.readObject();
	    	ois.close();
		} catch (Exception e) {e.printStackTrace();}
    	
		return ret;
	}
}
