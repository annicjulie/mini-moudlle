package managers;

import java.io.File;

import javafx.stage.Stage;
import main.Main;
import users.Utilisateur;
import windows.AccueilEleves;
import windows.AccueilProfs;
import windows.LogPage;

/**
 * Gere la connexion et la restriction des utilisateurs a leur environnement
 * @author lerouzyi
 *
 */

public class ConnectionManager {

	public static Utilisateur currentUser = null;
	
	/**
	 * 
	 * @param username le logname de l'utilisateur
	 * @param password le mot de passe
	 * @return true si la connexion a ete reussie, false sinon
	 */
	public boolean connect(String username, String password) {
		if(currentUser != null) {
			Main.logDebug("Il y a deja un utilisateur connecte");
			return false;
		}
		currentUser = Utilisateur.login(username, password);
		return currentUser != null;
	}
	
	/**
	 * sauvegarde l'utilisateur actuel et le deconnecte
	 */
	public void disconnect() {
		if(currentUser == null) {
			Main.logDebug("Aucun utilisateur connecte");
		} else {
			currentUser.writeToFS();
			currentUser = null;
		}
	}
	
	/**
	 * 
	 * @return l'utilisateur connecte actuellement
	 */
	public Utilisateur getCurrentUser() {
		return currentUser;
	}
	
	/**
	 * 
	 * @return la liste des utilisateurs (leurs lognames)
	 */
	public static String[] getUsers() {
		File f = new File("data/users/");
		return f.list();
	}
	
	/**
	 * 
	 * @return l'accueil qui correspond a l'utilisateur actuel, ou la page de login si l'utilisateur est null
	 */
	public Stage getAccueil() {
		if(currentUser == null) return new LogPage();
		else {
			if(currentUser.isProf()) return new AccueilProfs();
			else return new AccueilEleves();
		}
	}
}
