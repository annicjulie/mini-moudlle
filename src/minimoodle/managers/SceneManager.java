package managers;

import java.net.URL;

import javafx.stage.Stage;

/**
 * Classe SceneManager, c'est avec cette classe que l'on gère les différentes scènes.
 * @author leguyaai
 * @version 1.0
 */

public class SceneManager {
	
	private Stage currentStage;
	
	/**
	 * C'est le constructeur de la classe SceneManager
	 * @param stage Stage
	 * @since 1.0
	 */
	
	public SceneManager(Stage stage) {
		URL CSSfile = getClass().getResource("/res/style.css");
		if(CSSfile!=null) {
			stage.getScene().getStylesheets().add(CSSfile.toExternalForm());
		}
		this.currentStage = stage;
	}
	
	/**
	 * Cette méthode retourne le(la) Stage/Fenêtre actuelle.
	 * @since 1.0
	 * @return Stage currentStage
	 */
	
	public Stage getStage() {
		return this.currentStage;
	}
	
	/**
	 * Cette méthode s'occupe de changer la Scene dans la fenêtre principale.
	 * @since 1.0
	 * @param stage Stage
	 */
	
	public void changeScene(Stage stage) {
		URL CSSfile = getClass().getResource("/res/style.css");
		this.currentStage.setTitle(stage.getTitle());
		this.currentStage.setWidth(stage.getWidth());
		this.currentStage.setHeight(stage.getHeight());
		this.currentStage.setScene(stage.getScene());
		if(CSSfile!=null) {
			this.currentStage.getScene().getStylesheets().add(CSSfile.toExternalForm());
		}
	}
	
}
