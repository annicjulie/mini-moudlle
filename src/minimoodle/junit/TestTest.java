package junit;

import java.util.ArrayList;

import junit.framework.TestCase;
import question.Choix;
import question.QCM;
import question.QRT;
import question.Question;
import question.Test;

public class TestTest extends TestCase {
	
	public TestTest(String testName) {
        super(testName);
    }
	
	protected void setUp() throws Exception {
        super.setUp();
    }
	
	protected void tearDown() throws Exception {
        super.tearDown();
    }
	
	public void testResult() {
		Test t = new Test("Test", new ArrayList<Question>());
		assertTrue(t.getResult() > -1);
	}
	
	public void testCorrection() {
		ArrayList<Question> questions = new ArrayList<Question>();
		QRT qrt = new QRT("Intitulé");
		ArrayList<Choix> choiceList = new ArrayList<Choix>();
		Choix c1 = new Choix("Bonne réponse", true);
		Choix c2 = new Choix("Mauvaise réponse", false);
		choiceList.add(c1);
		choiceList.add(c2);
		QCM qcm = new QCM("QCM", true, choiceList);
		qrt.setScore(Question.NOT_CORRECTED);
		qcm.setScore(1);
		questions.add(qrt);
		questions.add(qcm);
		Test test = new Test("Intitulé", questions);
		assertFalse(test.isCorrected());
	}
}
