package junit;

import junit.framework.TestCase;
import users.Utilisateur;

public class UtilisateurTest extends TestCase {
	
	public UtilisateurTest(String testName) {
        super(testName);
    }
	
	protected void setUp() throws Exception {
        super.setUp();
    }
	
	protected void tearDown() throws Exception {
        super.tearDown();
    }
	
	public void testInstanceLogname() {
		Utilisateur u = new Utilisateur("Test", "TEST", "test", true);
		assertTrue(u.getLogname() instanceof String);
	}
	
	public void testIsStudentOrProfesor() {
		Utilisateur u = new Utilisateur("Test", "TEST", "test", true);
		Utilisateur u2 = new Utilisateur("Test", "TEST", "test", false);
		assertTrue(u.isProf());
		assertFalse(u2.isProf());
	}
}
