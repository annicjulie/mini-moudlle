package junit;

import java.util.ArrayList;

import junit.framework.TestCase;
import question.Choix;
import question.QCM;

public class QCMTest extends TestCase {
	
	public QCMTest(String testName) {
        super(testName);
    }
	
	protected void setUp() throws Exception {
        super.setUp();
    }
	
	protected void tearDown() throws Exception {
        super.tearDown();
    }
	
	public void testXor() {
		ArrayList<Choix> choiceList = new ArrayList<Choix>();
		Choix c1 = new Choix("Bonne réponse", true);
		Choix c2 = new Choix("Mauvaise réponse", false);
		choiceList.add(c1);
		choiceList.add(c2);
		QCM qcm = new QCM("QCM", true, choiceList);
		assertTrue(qcm.isXor());
	}
	
	public void testInstance() {
		ArrayList<Choix> choiceList = new ArrayList<Choix>();
		Choix c1 = new Choix("Bonne réponse", true);
		Choix c2 = new Choix("Mauvaise réponse", false);
		choiceList.add(c1);
		choiceList.add(c2);
		QCM qcm = new QCM("QCM", true, choiceList);
		assertTrue(qcm instanceof QCM);
	}

}
