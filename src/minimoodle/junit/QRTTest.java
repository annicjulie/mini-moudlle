package junit;

import junit.framework.TestCase;
import question.QRT;

public class QRTTest extends TestCase {
	
	public QRTTest(String testName) {
        super(testName);
    }
	
	protected void setUp() throws Exception {
        super.setUp();
    }
	
	protected void tearDown() throws Exception {
        super.tearDown();
    }
	
	public void testScore() {
		QRT qrt = new QRT("Intitulé");
		assertFalse(qrt.getScore() > 0);
	}

}
