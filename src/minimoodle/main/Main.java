package main;

import java.io.File;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.stage.Stage;
import managers.ConnectionManager;
import managers.SceneManager;
import managers.TestManager;
import question.Choix;
import question.QCM;
import question.QRT;
import question.QuestionList;
import users.Utilisateur;
import windows.LogPage;

public class Main extends Application {
	
	private static SceneManager sceneManager;
	private static ConnectionManager connectionManager;
	private static TestManager testManager;
	
	public static final int WIDTH = 1280, HEIGHT = 720;
	public static final boolean RESIZABLE = false;
	
	private static boolean logDebug = false;
	
	public static void logDebug(String message) {
		if(logDebug) System.out.println(message);
	}
	
	public void start(Stage window) throws Exception {
		connectionManager = new ConnectionManager();
        testManager = new TestManager();
        sceneManager = new SceneManager(new LogPage());
		sceneManager.getStage().show();
	}
	
	public static void main(String[] args) {
		System.out.println("Arguments possibles :\n-setup : Crée le dossier data et toutes les configurations\n-debug : Affiche les messages de debug");
		File dataDir = new File("data");
		if(!dataDir.exists()) setup();
		if(args.length > 0) {
			for(int i = 0; i < args.length; i++) {
				if(args[i].equals("-setup")) setup();
				else if(args[i].equals("-debug")) logDebug = true;
			}
		}
		Application.launch(args);
	}
	
	private static void setup() {
		QuestionList ql = new QuestionList("Exemple");
		ArrayList<Choix> choiceList = new ArrayList<Choix>();
		Choix c1 = new Choix("Bonne réponse", true);
		Choix c2 = new Choix("Mauvaise réponse", false);
		choiceList.add(c1);
		choiceList.add(c2);
		ql.addQuestion(new QRT("Ceci est un exemple de question"), 0);
		ql.addQuestion(new QCM("QCM", true, choiceList), 1);
		try {
			QuestionList.writeToFS();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Utilisateur prof = new Utilisateur("Prof", "Prof", "prof", true);
		prof.writeToFS();
		Utilisateur eleve = new Utilisateur("Eleve", "Eleve", "eleve", false);
		eleve.writeToFS();
	}

	public static SceneManager getSceneManager() {
		return sceneManager;
	}
	
	public static ConnectionManager getConnectionManager() {
		return connectionManager;
	}
	
	public static TestManager getTestManager() {
		return testManager;
	}
}

